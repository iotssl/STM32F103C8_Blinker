/**
 * Blinker für die BluePill
 */

#include <stdint.h>
#include <stdio.h>
#include "stm32f1xx.h"
#include "init.h"   //Initialisierungen
#include "helper.h" //Hilfsfunktionen wie z.B. wait()

#define LED_OFF WRITE_REG(GPIOC->BSRR,GPIO_BSRR_BS13);
#define LED_ON  WRITE_REG(GPIOC->BSRR,GPIO_BSRR_BR13);


int main(void)
{
    // Initialize I/O pins

    init_io();

    // Initialize system timer
    SysTick_Config(SystemCoreClock/1000);

    while(1)
    {
        // LED Off
        LED_OFF;
        puts("Led aus!");
        // Delay 1 second
        wait(1);
        // LED On
        LED_ON;
        puts("Ich habe geblinkt!");
        // Delay 1 second
        wait(1);
    }
}
